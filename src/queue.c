#include <stdbool.h>
#include <stdlib.h>

#include "queue.h"

struct QNode *newNode(int num)
{
    struct QNode *temp = (struct QNode *)malloc(sizeof(struct QNode));
    if(NULL == temp) {
        return NULL;
    }

    temp->num = num;
    temp->next = NULL;
    return temp;
}

struct Queue *createQueue(void)
{
    struct Queue *q = (struct Queue *)malloc(sizeof(struct Queue));
    if(NULL == q) {
        return NULL;
    }

    q->front = NULL;
    q->rear = NULL;
    return q;
}

void enQueue(struct Queue *q, struct QNode *temp)
{
    // If queue is empty, then new node is both front and rear.
    if (NULL == q->rear) {
        q->front = temp;
        q->rear = temp;
    } else {
        // Add the new node at the end of queue.
        q->rear->next = temp;
        q->rear = temp;
    }
}

int deQueue(struct Queue *q)
{
    // If queue is empty, return NULL.
    if (NULL == q->front) {
        return -1;
    }

    // Store previous front and move front one node ahead.
    struct QNode *temp = q->front;

    q->front = q->front->next;

    // If front becomes NULL, then change rear also as NULL.
    if (NULL == q->front) {
        q->rear = NULL;
    }

    int num = temp->num;
    free(temp); // free the node.
    return num;
}

bool is_empty(const struct Queue *q)
{
    if(NULL == q || (NULL == q->front && NULL == q->rear)) {
        return true;
    }

    return false;
}