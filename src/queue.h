#ifndef QUEUE_H
#define QUEUE_H

struct QNode {
    int num;
    struct QNode *next;
};

// struct queue
struct Queue {
    struct QNode *front, *rear;
};

// Add a new node
struct QNode *newNode(int clifd);

struct Queue *createQueue(void);

// Enqueue new node to queue
void enQueue(struct Queue *q, struct QNode *temp);

// Enqueue new node to queue
int deQueue(struct Queue *q);

bool is_empty(const struct Queue *q);

#endif // QUEUE_H
